
########################
###    Data source   ###
########################

data "aws_ami" "techtide_app" {
  most_recent = true


  filter {
    name   = "name"
    values = ["techtide-app-ami"]
  }
}

data "aws_ami" "techtide_web" {
  most_recent = true


  filter {
    name   = "name"
    values = ["techtide-web-ami"]
  }
}
########################
###    Ec2 Instance  ###
########################


resource "aws_instance" "PublicWebTemplate" {
  #ami                    = data.aws_ami.amazon_linux_2.id
  ami                    = data.aws_ami.techtide_web.id
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.public-web-subnet-1.id
  vpc_security_group_ids = [aws_security_group.webserver-security-group.id]
  key_name               = "source_key"
  #  user_data              = file("setup-web.sh")
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name

  tags = {
    Name = "WebTier"
  }
}

resource "aws_instance" "private-app-template" {
  #ami = data.aws_ami.amazon_linux_2.id
  ami                    = data.aws_ami.techtide_app.id
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.private-app-subnet-1.id
  vpc_security_group_ids = [aws_security_group.ssh-security-group.id]
  key_name               = "source_key"
  # user_data              = file("setup-app.sh")
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name

  tags = {
    Name = "AppTier"
  }
}


############################
##   Web app load balancer ##
#############################
resource "aws_lb" "application-load-balancer" {
  name                       = "public-load-balancer"
  internal                   = false
  load_balancer_type         = "application"
  security_groups            = [aws_security_group.alb-security-group.id]
  subnets                    = [aws_subnet.public-web-subnet-1.id, aws_subnet.public-web-subnet-2.id]
  enable_deletion_protection = false

  tags = {
    Name = "App load balancer"
  }
}

resource "aws_lb_target_group" "alb_target_group" {
  name     = "WebTierTG"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc_01.id
}

resource "aws_lb_target_group_attachment" "web-attachment" {
  target_group_arn = aws_lb_target_group.alb_target_group.arn
  target_id        = aws_instance.PublicWebTemplate.id
  port             = 80
}

# create a listener on port 80 with redirect action
resource "aws_lb_listener" "alb_http_listener" {
  load_balancer_arn = aws_lb.application-load-balancer.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb_target_group.arn
  }
}


############################
##   Internal load balancer ##
#############################
resource "aws_lb" "internal-load-balancer" {
  name                       = "app-load-balancer"
  internal                   = true
  load_balancer_type         = "application"
  security_groups            = [aws_security_group.internal-lb-sg.id]
  subnets                    = [aws_subnet.private-app-subnet-1.id, aws_subnet.private-app-subnet-2.id]
  enable_deletion_protection = false

  tags = {
    Name = "App load balancer"
  }
}

resource "aws_lb_target_group" "app_target_group" {
  name     = "AppTierTG"
  port     = 4000
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc_01.id
  health_check {
    path = "/health"
  }
}

resource "aws_lb_target_group_attachment" "app-attachment" {
  target_group_arn = aws_lb_target_group.app_target_group.arn
  target_id        = aws_instance.private-app-template.id
  port             = 4000
}

resource "aws_lb_listener" "app_http_listener" {
  load_balancer_arn = aws_lb.internal-load-balancer.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app_target_group.arn
  }
}

#################################
####  database subnet group #####
#################################
resource "aws_db_subnet_group" "database-subnet-group" {
  name        = "database subnets"
  subnet_ids  = [aws_subnet.private-db-subnet-1.id, aws_subnet.private-db-subnet-2.id]
  description = "Subnet group for database instance"

  tags = {
    Name = "Database Subnets"
  }
}
#################################
####  database instance      #####
#################################
resource "aws_db_instance" "database-instance" {
  allocated_storage      = 10
  identifier             = "techtidedb"
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = var.database-instance-class
  db_name                = "webappdb"
  username               = "admin"
  password               = "admin"
  parameter_group_name   = "default.mysql5.7"
  skip_final_snapshot    = true
  availability_zone      = "us-east-1b"
  db_subnet_group_name   = aws_db_subnet_group.database-subnet-group.name
  multi_az               = var.multi-az-deployment
  vpc_security_group_ids = [aws_security_group.database-security-group.id]
}

resource "aws_iam_instance_profile" "ec2_profile" {
  name = "ec2_profile"
  role = aws_iam_role.ec2_role.name
}
